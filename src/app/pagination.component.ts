/**
 * Created by saishav7 on 20/1/17.
 */

import {Component, Input, EventEmitter, Output, OnChanges} from "@angular/core";

@Component({
  selector: 'pagination',
  template: `
  <nav *ngIf="itemsLength > pageSize">
  <ul class="pagination">
    <li [class.disabled]="currentPage == 1">
      <a (click)="previous()" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li [class.active]="currentPage == page" (click)="changePage(page)" *ngFor="let page of pages">
      <a>{{ page }}</a>
    </li>
    
    <li [class.disabled]="currentPage == pages.length">
      <a (click)="next()" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
`
})

export class PaginationComponent implements OnChanges {

  @Input('items-length') itemsLength = 0;
  @Input('page-size') pageSize = 10;
  @Output('page-changed') pageChanged = new EventEmitter();
  pages: any[];
  currentPage;

  ngOnChanges() {
    this.currentPage = 1;
    var pagesCount = this.itemsLength / this.pageSize;
    this.pages = [];
    for (var i = 1; i <= pagesCount; i++)
      this.pages.push(i);
  }

  previous() {
    if (this.currentPage == 1) {
      return;
    }

    this.currentPage--;
    this.pageChanged.emit(this.currentPage);
  }

  next() {
    if (this.currentPage == this.pages.length) {
      return;
    }

    this.currentPage++;
    this.pageChanged.emit(this.currentPage);
  }

  changePage(page) {
    this.currentPage = page;
    this.pageChanged.emit(page);
  }

}
