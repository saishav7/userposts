import {Injectable} from "@angular/core";
import 'rxjs/add/operator/map';
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import {Post} from "./post";

/**
 * Created by saishav7 on 15/1/17.
 */


@Injectable()
export class PostsService {

  private postsUrl = 'http://jsonplaceholder.typicode.com/posts';


  constructor(private http: Http) {}

  getPosts(filter?): Observable<Post[]> {
    var url = this.postsUrl;
    if(filter && filter.userId) {
      url = url + '?userId=' + filter.userId;
    }
      return this.http.get(url).map(response => response.json());
  }

  getComments(postId): Observable<Comment[]> {
    return this.http.get(this.postsUrl + '/' + postId + '/comments').map(response => response.json());
  }

}
