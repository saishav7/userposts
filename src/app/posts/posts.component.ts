/**
 * Created by saishav7 on 6/1/17.
 */
import {Component, OnInit} from '@angular/core';
import {PostsService} from "./posts.service";
import {Post} from "./post";
import {UsersService} from "../users/users.service";
import * as _ from 'underscore';

@Component({
  selector: 'posts',
  template: `
  <h1>Posts</h1>
  <div class="col-md-6">
  
    <select #u (change)="loadPosts({ userId:u.value })">
     <option value="">Select user</option>
      <option value="{{user.id}}" *ngFor="let user of users">{{user.name}}</option>
    </select>
  
    <pagination [items-length]="posts.length" [page-size]="pageSize" (page-changed)="onPageChanged($event)"></pagination>
    <loader [visible]="isLoadingPosts"></loader>
    <ul class="posts list-group">
      <li (click)="activatePost(post.id)" [class.active]="isActive(post)" class="list-group-item" *ngFor="let post of pagedPosts">
          {{post.title}}
      </li>
    </ul>
  </div>
  <div [hidden]="isEmptyObject(activePost)" class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading">
          <h3 class="panel-title">{{activePost.title}}</h3>
      </div>
      <div class="panel-body">
        {{activePost.body}}
      </div>
    </div>
    <loader [visible]="isLoadingComments"></loader>
    <div [hidden]="isEmptyObject(postComments)" *ngFor="let postComment of postComments" class="media">
      <div class="media-left">
        <a href="#">
          <img class="media-object thumbnail" src="http://lorempixel.com/80/80/people?random={{postComment.id}}" >
        </a>
      </div>
      <div class="media-body">
        <h4 class="media-heading">{{postComment.name}}</h4>
        {{postComment.body}}
      </div>
    </div>
  </div>
`,
  styles:[`
  .media-object {border-radius:100%}
  .posts	li	{	cursor:	default;	}
  .posts	li:hover	{	background:	#ecf0f1;	}	
  .list-group-item.active,	
  .list-group-item.active:hover,	
  .list-group-item.active:focus	{	
     background-color:	#ecf0f1;
     border-color:	#ecf0f1;	
     color:	#2c3e50;
  }
`]
})
export class PostsComponent implements OnInit {
  private posts = [];
  private isLoadingPosts = true;
  private isLoadingComments = true;
  private activePost : Post = new Post();
  private postComments : Comment[] = new Array();
  private users;
  private pagedPosts;
  private pageSize = 5;

  constructor(private postsService : PostsService, private userService : UsersService) { }

  ngOnInit() {
    this.loadPosts();
    this.loadUsers();
  }

  activatePost(id) {
    for(let post of this.posts) {
      if (post.id == id) {
        this.activePost = post;
      }
    }
    this.postsService.getComments(id).subscribe(comments => {
      this.postComments = comments;
      this.isLoadingComments = false;
    });

  }

  isEmptyObject(obj) {
    return (Object.keys(obj).length === 0);
  }

  isActive(post : Post) {
    return (this.activePost.id == post.id);
  }

  loadPosts(filter?) {
    this.isLoadingPosts = true;
    this.postsService.getPosts(filter).subscribe(posts => {
      this.posts = posts;
      this.pagedPosts = _.take(this.posts, this.pageSize);
    });
    this.isLoadingPosts = false;
  }

  loadUsers() {
    this.userService.getUsers().subscribe(users => {
      this.users = users;
    });
  }

  onPageChanged(page) {
    var startingIndex = (page - 1) * this.pageSize;
    this.pagedPosts =  _.take(_.rest(this.posts, startingIndex), this.pageSize);
  }
}

