/**
 * Created by saishav7 on 15/1/17.
 */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import {routing} from "../app.routing";
import {PreventUnsavedChangesGuard} from "../prevent-unsaved-changes-guard.service";
import {PostsComponent} from "./posts.component";
import {PostsService} from "./posts.service";
import {LoaderComponent} from "../loader.component";
import {PaginationComponent} from "../pagination.component";

@NgModule({
  declarations: [
    PostsComponent,
    LoaderComponent,
    PaginationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    ReactiveFormsModule
  ],
  providers: [PostsService, PreventUnsavedChangesGuard],
})
export class PostsModule { }

