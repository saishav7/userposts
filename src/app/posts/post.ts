export class Post {
  userId : string;
  id : string;
  title : string;
  body : string;
}

export class Comment {
  postId : string;
  id : string;
  name : string;
  email : string;
  body : string;
}
