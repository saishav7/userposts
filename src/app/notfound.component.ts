/**
 * Created by saishav7 on 6/1/17.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'not-found',
  template: `
  <h1>Invalid link</h1>
`
})
export class NotFoundComponent {
}
