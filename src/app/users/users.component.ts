/**
 * Created by saishav7 on 6/1/17.
 */
import {Component, OnInit} from '@angular/core';
import {UsersService} from "./users.service";


@Component({
  selector: 'users',
  template: `
  <div style="margin-left:45px;">
    <h1>Users</h1>
    <p>
      <a class="btn btn-primary" href="/users/add">Add User</a>
    </p>
    <table class="table table-bordered">
   
        <tr *ngFor="let user of users">
          <td>{{ user.name }}</td>
          <td>{{ user.email }}</td>
          <td>
          <a [routerLink]="['/users', user.id]">
            <i class="glyphicon glyphicon-edit"></i>
          </a>  
          </td>
          <td>
            <i (click)="deleteUser(user)" class="glyphicon glyphicon-remove clickable"></i>
          </td>
        </tr>
    </table>
  </div>  
  `
})
export class UsersComponent implements OnInit {

  private users;

  constructor(private userService : UsersService) { }

  ngOnInit() {
    this.userService.getUsers().subscribe(users => {
          this.users = users;
        });
  }

  deleteUser(user){
    if (confirm("Are you sure you want to delete " + user.name + "?")) {
      var index = this.users.indexOf(user)
      // Here, with the splice method, we remove 1 object
      // at the given index.
      this.users.splice(index, 1);

      this.userService.deleteUser(user.id)
        .subscribe(null,
          err => {
            alert("Could not delete the user.");
            this.users.splice(index, 0, user);
          });
    }
  }


}
