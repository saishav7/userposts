import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import {User} from "./user";
import 'rxjs/add/operator/map';
/**
 * Created by saishav7 on 7/1/17.
 */

@Injectable()
export class UsersService {

  private users = 'http://jsonplaceholder.typicode.com/users';


  constructor(private http: Http) {}

  getUsers(): Observable<User[]> {
    return this.http.get(this.users).map(response => response.json());
  }

  getUser(id): Observable<User> {
    return this.http.get(this.users + "/" + id).map(response => response.json());
  }

  addUser(user : User) {
    return this.http.post(this.users, user).map(response => response.json());
  }

  updateUser(user : User) {
    return this.http.put(this.users+ "/" + user.id, user).map(response => response.json());
  }

  deleteUser(id) {
    return this.http.delete(this.users + "/" + id).map(response => response.json());
  }


}
