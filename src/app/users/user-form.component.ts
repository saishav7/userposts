/**
 * Created by saishav7 on 7/1/17.
 */
import {Component, OnInit} from '@angular/core';
import {UsersService} from "./users.service";
import {FormGroup, FormBuilder, Validators}   from '@angular/forms';
import {EmailValidators} from "../emailValidators";
import {FormComponent} from "../prevent-unsaved-changes-guard.service";
import {Router, ActivatedRoute} from "@angular/router";
import {User} from "./user";

@Component({
  selector: 'users',
  templateUrl: './user-form.component.html',
  providers: [UsersService]
})

export class UserFormComponent implements FormComponent, OnInit {

  form : FormGroup;
  title;

  user = new User();

  constructor(private userService: UsersService,
              fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute) {
    this.form = fb.group({
      name: ['', Validators.required],
      email: ['', Validators.compose([EmailValidators.validateEmail])],
      phone: [],
      address: fb.group({
        street: [],
        suite: [],
        city: [],
        zipcode: []
        })
    });
  }

  ngOnInit() {

    var id = this.route.params.subscribe(params => {
      var id = +params['id'];
      this.title = id ? "Edit User" : "New User";

      if(!id) {
        return;
      }
      this.userService.getUser(id).subscribe(
        user => this.user = user,
        response => {
          if(response.status == 404) {
            this.router.navigate(['/notfound']);
          }
        }
      );
    })
  }

  save() {
    var result;
    if (this.user.id) {
      result = this.userService.updateUser(this.user);
    } else {
      result = this.userService.addUser(this.form.value)
    }
    result.subscribe(x => {
      this.form.markAsPristine();
      this.router.navigate(['/users']);
    })
  }

}
