/**
 * Created by saishav7 on 7/1/17.
 */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import {routing} from "../app.routing";
import {UsersComponent} from "./users.component";
import {UserFormComponent} from "./user-form.component";
import {UsersService} from "./users.service";
import {PreventUnsavedChangesGuard} from "../prevent-unsaved-changes-guard.service";

@NgModule({
  declarations: [
    UsersComponent,
    UserFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    ReactiveFormsModule
  ],
  providers: [UsersService, PreventUnsavedChangesGuard],
})
export class UsersModule { }
