import {FormControl} from "@angular/forms";
/**
 * Created by saishav7 on 8/1/17.
 */
export class EmailValidators {

  static validateEmail(control: FormControl) {

    if (!isValid(control.value)) {
      return { validateEmail: true };
    }

    function isValid(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }
  }
}
