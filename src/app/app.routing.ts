/**
 * Created by saishav7 on 6/1/17.
 */
import {Router, RouterModule} from '@angular/router'
import {UsersComponent} from "./users/users.component";
import {PostsComponent} from "./posts/posts.component";
import {NotFoundComponent} from "./notfound.component";
import {HomeComponent} from "./home.component";
import {UserFormComponent} from "./users/user-form.component";
import {PreventUnsavedChangesGuard} from "./prevent-unsaved-changes-guard.service";

export const routing = RouterModule.forRoot([
  { path:'', component: HomeComponent },
  { path:'users/add', component:UserFormComponent, canDeactivate: [PreventUnsavedChangesGuard]},
  { path:'users/:id', component:UserFormComponent },
  { path:'users', component:UsersComponent },
  { path:'posts', component:PostsComponent },
  { path:'**', component:NotFoundComponent }
]);
