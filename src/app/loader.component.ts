import {Component, Input} from "@angular/core";
/**
 * Created by saishav7 on 15/1/17.
 */


@Component({
  selector: 'loader',
  template: `
  <i *ngIf="visible" class="fa fa-spinner fa-spin fa-5x fa-fw" aria-hidden="true"></i>
`
})
export class LoaderComponent {
  @Input() visible;
}
