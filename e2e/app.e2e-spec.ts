import { UserpostsPage } from './app.po';

describe('userposts App', function() {
  let page: UserpostsPage;

  beforeEach(() => {
    page = new UserpostsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
